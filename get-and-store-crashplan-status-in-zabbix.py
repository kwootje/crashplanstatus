#!/usr/bin/env python3
# -*- coding: utf-8 -*
#
# Copyright 2018 i2rs
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Python3(!) program to check te status of all backups on CrashPlan
# - retrieve list of active servers
# - retrieve info per server
# - store in Zabbix server
#
# This program was developed by Jeroen Baten <jbaten@i2rs.nl>.
# Development was sponsored by the Aurum Europe company.
#
# How to use the script:
# 0. Read the README file.
# 1. Create a virtual environment containing modules mentioned in requirements.txt
# 2. Run the script. Good luck....

import requests
import sys
import configparser
from requests.auth import HTTPBasicAuth
from datetime import date, datetime
from tzlocal import get_localzone

destination_project_name = "Cleanup"
destination_column_name = "To do"

# read config file
configFile = "get-and-store-crashplan-status-in-zabbix.conf"
config = configparser.RawConfigParser()
try:
    config.read(configFile)
    # Read config vars
    CRASHPLAN_URL = config.get('settings', 'crashplan_url')
    CRASHPLAN_PORT = config.get('settings', 'crashplan_port')
    CRASHPLAN_USER = config.get('settings', 'crashplan_user')
    CRASHPLAN_PASSWORD = config.get('settings', 'crashplan_password')

    # Not implemented yet
    ZABBIX_SERVER = config.get('settings', 'zabbix_server')

except:
    print("Error reading configfile '" + configFile + "'.")
    print("Program aborted.")
    sys.exit(1)


def get_backup_status_from_crashplan():
    """
    Make a REST API call to crashplan server and return result
    :return: object with results received
    """
    # https: // www.crashplan.com / apidocviewer /
    # requests: basic auth: auth=HTTPBasicAuth('user', 'pass'))
    # GET https://www.crashplanpro.com/api/Computer/646023448578941377?idType=guid&incAll=true
    # Look under backupUsage for percentComplete

    urlparts = (str(CRASHPLAN_URL) + ":" + str(CRASHPLAN_PORT),
                "api/Computer?active=true")
    url = "/".join(urlparts)

    # pprint(url)
    result = requests.get(url, auth=HTTPBasicAuth(
        CRASHPLAN_USER, CRASHPLAN_PASSWORD))
    # pprint(result)
    return result


def convertCrashPlanTimestampToDatetimeObject(crashplanTimestamp):
    """
    Remove an unwanted ":" from the timezone info in a timestamp.
    :param crashplanTimestamp: String with timestamp
    :return: timestamp string without the ":" in the timezone info.
    """
    # hack the : out of the timezone info str(dt.astimezone(get_localzone()))with ''.join(iso_ts.rsplit(':', 1))
    crashplanTimestamp = ''.join(crashplanTimestamp.rsplit(':', 1))
    dt = datetime.strptime(crashplanTimestamp, '%Y-%m-%dT%H:%M:%S.%f%z')
    return dt


def get_backup_status_from_crashplan_for_computer(computerid):
    """
    Retrieve backup status from CrashPlan for a given computer id.
    :param computerid: computer id to look up.
    :return: object containing the results.
    """
    # https: // www.crashplan.com / apidocviewer /
    # This is not the best info in the world. A lot of it is deprecated, making searching for working calls difficult.
    # GET https://www.crashplanpro.com/api/Computer/646093248523141377?idType=guid&incAll=true
    urlparts = (str(CRASHPLAN_URL) + ":" + str(CRASHPLAN_PORT),
                "api/Computer/"+str(computerid)+"?idType=guid&incAll=true")
    url = "/".join(urlparts)
    result = requests.get(url, auth=HTTPBasicAuth(
        CRASHPLAN_USER, CRASHPLAN_PASSWORD))
    return result


def displayAndLog(handle, text):
    """
    Simple routine to both display and add to logfile
    :param handle: file handle of logfile.
    :param text: text to log.
    :return:
    """
    handle.write("# " + text + "\n")
    print(text)
    return


def minutessincetimestamp(crashplantimestamp):
    """
    Routine to calculate the amount of minutes since a given timestamp
    :param crashplantimestamp: timestamp
    :return: amount of minutes since a given timestamp
    """
    dt = convertCrashPlanTimestampToDatetimeObject(crashplantimestamp)
    displayAndLog(f, " -> Last connected adapted to local timezone: " +
                  str(dt.astimezone(get_localzone())))
    displayAndLog(f, " -> My current time is: " +
                  str(datetime.now(get_localzone())))
    timediff = (datetime.now(get_localzone()) - dt.astimezone(get_localzone()))
    minutesSinceCrashPlanTimestamp = round(timediff.seconds / 60)
    displayAndLog(f, " -> Calculated minutes: " +
                  str(minutesSinceCrashPlanTimestamp))
    return minutesSinceCrashPlanTimestamp


def addzabbixcommand(f, host, key, value):
    """
    Small routine to write zabbix_sender command to output file (usually zabbix-script.sh).
    :param f: file handle
    :param host: name of host as mentioned in zabbix to add the info to.
    :param key: key defined as a trapper item within the host.
    :param value: value to store.
    :return: No return value.
    """
    # working example:"zabbix_sender -vv  -c /etc/zabbix/zabbix_agentd.conf   -s "srv1-trapper"  --key crashplanpro.lastcompletedbackup --value "0001-01-01"
    f.write("zabbix_sender -vv -c /etc/zabbix/zabbix_agentd.conf --host " +
            host + " --key " + key + " --value " + value + "\n")
    return


print("Start")
print("=====")
with open("zabbix-script.sh", "w") as f:
    f.write("#!/bin/bash\n")
    f.write("# Version: 1.5\n")
    f.write("""
            # Checklist to avoid common pitfalls:
            # -Check that ServerActive parameter in zabbix_agentd.conf points to correct Zabbix server.
            # -Check that host ip of host sending the value is in 'allowed hosts' parameter of trapper item.
            # NOTE: Turns out that because our hosts are behind proxies, it is impossible to add data to their crashplanpro key items.
            # Solution as documented here: https://www.zabbix.com/forum/zabbix-troubleshooting-and-problems/29208-zabbix_sender-from-different-host
            # is to create separate host items (example srv1-trapper) and add the crashplan template there.
            """)
    displayAndLog(f, " -> My current time is: " +
                  str(datetime.now(get_localzone())))

    result = get_backup_status_from_crashplan()
    if result.status_code == 200:
        # pprint(result.json())
        data = result.json()["data"]
        # pprint(data)
        for computer in data["computers"]:

            # Because our hosts are behind proxies, but this runs on the zabbix server, this only
            # works if we create separate <host>-trapper hosts and add the template there.
            # If this is not in your case you can remove the "-trapper" part in the next line.
            host = computer["name"].split('.')[0] + "-trapper"
            displayAndLog(f, "Server: " + computer["name"] + " using: " + host)
            displayAndLog(f, " -> Software version: " +
                          computer["productVersion"])
            addzabbixcommand(f, host, "crashplanpro.scriptruntimestamp",
                             "\"" + str(datetime.now(get_localzone())) + "\"")

            displayAndLog(
                f, " -> Last connected according to CrashPlan: " + computer["lastConnected"])
            minutesSinceLastConnect = minutessincetimestamp(
                computer["lastConnected"])

            dt = convertCrashPlanTimestampToDatetimeObject(
                computer["lastConnected"])
            displayAndLog(f, " -> Last connected adapted to local timezone: " +
                          str(dt.astimezone(get_localzone())))
            addzabbixcommand(f, host, "crashplanpro.lastconnected",
                             "\"" + str(dt.astimezone(get_localzone())) + "\"")

            displayAndLog(
                f, " -> Minutes since last connection (for Zabbix): " + str(minutesSinceLastConnect))
            addzabbixcommand(f, host, "crashplanpro.minutessincelastconnection", str(
                minutesSinceLastConnect))

            result2 = get_backup_status_from_crashplan_for_computer(
                computer["guid"])
            if result2.status_code == 200:
                data2 = result2.json()["data"]
                usage = data2["backupUsage"]
                displayAndLog(f, " -> Current Percent Complete: " +
                              str(usage[0]["percentComplete"]))
                addzabbixcommand(f, host, "crashplanpro.percentcomplete", str(
                    usage[0]["percentComplete"]))

                lastCompletedBackup = usage[0]["lastCompletedBackup"]

                if lastCompletedBackup == "None" or lastCompletedBackup is None:
                    lastCompletedBackup = date.min
                    minutesSinceLastCompleteBackup = 9999
                else:
                    minutesSinceLastCompleteBackup = minutessincetimestamp(
                        lastCompletedBackup)

                displayAndLog(f, " -> Last Completed Backup: " +
                              str(lastCompletedBackup))
                addzabbixcommand(f, host, "crashplanpro.lastcompletedbackup",
                                 "\"" + str(lastCompletedBackup) + "\"")

                displayAndLog(f, " -> Minutes since last complete backup (for Zabbix): " +
                              str(minutesSinceLastCompleteBackup))
                addzabbixcommand(f, host, "crashplanpro.minutessincelastcompletebackup", str(
                    minutesSinceLastCompleteBackup))

f.close()
print("Done")
