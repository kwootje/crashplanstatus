#!/bin/bash

cd /opt/crashplanstatus

source venv/bin/activate

./get-and-store-crashplan-status-in-zabbix.py  > /tmp/zabbix-script.log 2>&1

/bin/bash -x ./zabbix-script.sh >> /tmp/zabbix-script.log 2>>/tmp/zabbix-script.err




