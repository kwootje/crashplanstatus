#!/bin/bash

source venv/bin/activate

echo "Generate requirements.txt file"
pip freeze  > requirements.txt

echo "5: You should check if files need to be added..."
git clean -f -n

echo "Last 4 tags:" 
git tag | tail -4l

read -p "Enter the name for this release : " rel

echo "Add to Changelog"
echo "${rel}" > /tmp/CHANGELOG.tmp
echo "==========" >> /tmp/CHANGELOG.tmp
git log --oneline > CHANGELOG
git log `git describe --tags --abbrev=0`..HEAD --oneline >> /tmp/CHANGELOG.tmp
echo >> /tmp/CHANGELOG.tmp
cat /tmp/CHANGELOG.tmp CHANGELOG > /tmp/CHANGELOG.tmp2
vim /tmp/CHANGELOG.tmp2
cp /tmp/CHANGELOG.tmp2 CHANGELOG
rm /tmp/CHANGELOG.tmp /tmp/CHANGELOG.tmp2

git commit -a -m "$rel"
git tag $rel
git push origin master --tags

